package com.cognizant.LearnTodayRESTAPI.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cognizant.LearnTodayRESTAPI.dao.CourseDao;
import com.cognizant.LearnTodayRESTAPI.dao.impl.CourseDaoImpl;
import com.cognizant.LearnTodayRESTAPI.model.Course;


public class AdminService {
	
	
	private CourseDaoImpl dao=new CourseDaoImpl();
	
	
	public List<Course> getAllCourses(){
		return dao.getAllCourses();
	}

	public List<Course> getCourseById(int id){
		
		return dao.getCourseById(id);
	}

}
