package com.cognizant.LearnTodayRESTAPI.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cognizant.LearnTodayRESTAPI.dao.CourseDao;
import com.cognizant.LearnTodayRESTAPI.dao.StudentDao;
import com.cognizant.LearnTodayRESTAPI.dao.exception.InvalidEnrollmentIdException;
import com.cognizant.LearnTodayRESTAPI.dao.impl.CourseDaoImpl;
import com.cognizant.LearnTodayRESTAPI.dao.impl.StudentDaoImpl;
import com.cognizant.LearnTodayRESTAPI.model.Course;
import com.cognizant.LearnTodayRESTAPI.model.Student;


public class StudentService {

	
	private StudentDao dao=new StudentDaoImpl();
	
	private CourseDao courseDao=new CourseDaoImpl();
	
	public List<Course> getAllCourses() {
		
		return courseDao.getAllCourses();
	}
	
	public int register(Student s){
		return dao.register(s);
	}
	public int delete(int id) throws InvalidEnrollmentIdException {
		return dao.delete(id);
	}
}
