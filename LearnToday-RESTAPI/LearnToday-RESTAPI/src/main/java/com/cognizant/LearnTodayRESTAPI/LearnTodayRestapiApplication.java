package com.cognizant.LearnTodayRESTAPI;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan("com.cognizant.LearnTodayRESTAPI")
public class LearnTodayRestapiApplication {

	public static void main(String[] args) {
		SpringApplication.run(LearnTodayRestapiApplication.class, args);
	}

}
