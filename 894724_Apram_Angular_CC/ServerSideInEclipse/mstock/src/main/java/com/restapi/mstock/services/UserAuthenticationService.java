package com.restapi.mstock.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.restapi.mstock.dao.UserDao;
import com.restapi.mstock.models.UserDetails;

@Service
public class UserAuthenticationService {

	@Autowired
	private UserDao userDao;
	public boolean validateUserLogin(String email,String password) {
		UserDetails existingUser = userDao.find(email);
		System.out.println(existingUser);
		if(existingUser!=null)
		{
			if(existingUser.getPassword().equals(password))
			{
				return true;
			}
		}
		return false;
	}
}
