package com.restapi.mstock.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.restapi.mstock.models.CompanyDetails;
import com.restapi.mstock.services.CompanyStockServices;


@RestController
@RequestMapping("api/stock")
@CrossOrigin
public class CompanyStockController{
	@Autowired
	private CompanyStockServices companyStockServices;
	
	@GetMapping("/companystocks")
	@ResponseStatus(HttpStatus.OK)
	public List<CompanyDetails> companyStockDetails()
	{
		return companyStockServices.getCompanyStockDetails();
	}
}
